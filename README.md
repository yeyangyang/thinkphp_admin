# MEAdmin

`MEAdmin` 是一个基于 `Thinkphp5.1` + `bootstrap4` 开发的后台管理系统，集成后台系统常用功能。

项目安装请参考ThinkPHP官方文档及下面的服务环境说明，数据库sql文件存放于项目根目录下。

## 服务器环境

- PHP版本不低于 `PHP5.5`，推荐使用 `PHP7+` 以达到最优效果

## 后台菜单
```
后台
├─后台主页
├─系统管理              
│  ├─系统参数配置
│  ├─系统日志管理
│  ├─附件管理
│  └─数据库管理
│
├─权限管理
│  ├─系统角色管理
│  ├─系统权限管理
│  └─系统用户管理
│
├─扩展管理
│  └─钩子管理
└─.没有了
```


## 鸣谢
感谢[ThinkPHP](http://www.thinkphp.cn/)、[JQuery](https://jquery.com/)、[bootstrap](https://getbootstrap.net/)等优秀开源项目。


## 项目版本

体验账号和密码 `admin`/`123456`

> 体验地址：[http://admin.meetes.cn/](http://admin.meetes.cn/)

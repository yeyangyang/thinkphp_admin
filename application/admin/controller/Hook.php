<?php
// +----------------------------------------------------------------------
// | MEAdmin [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2020 http://www.meetes.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 扯文艺的猿 <meetes@163.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use app\common\controller\Admin;

/**
 * 钩子控制器
 * @package app\admin\controller
 */
class Hook extends Admin
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        // 待开发
        $this->error('无法使用，待开发');
    }

}

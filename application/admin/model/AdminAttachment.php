<?php
// +----------------------------------------------------------------------
// | MEAdmin [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2020 http://www.meetes.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 扯文艺的猿 <meetes@163.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 后台附件管理模型类
// +----------------------------------------------------------------------

namespace app\admin\model;

use think\Model;

class AdminAttachment extends Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = true;


}

<?php
// +----------------------------------------------------------------------
// | MEAdmin [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2020 http://www.meetes.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 扯文艺的猿 <meetes@163.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 应用公共文件
// +----------------------------------------------------------------------
use think\Db;

/**
 * 操作成功/失败返回的接口格式  原型方法 json_encode($array,JSON_UNESCAPED_UNICODE) 
 * @access protected
 * @param  bool      $status 状态 true为success/false为error
 * @param  mixed     $msg 提示信息
 * @param  array     $data 返回的数据
 * @param  integer   $code 状态码
 * @param  string    $url 跳转的URL地址
 * @return 返回json字符串
 */
function apiRule($status = false, $msg = "", $data = null, $code = 0, $url = "")
{
    $result = [
        'status' => $status ? 'success' : 'error',
        'msg'  => $msg,
        'data' => $data,
        'code' => $status ? 200 : (int)$code,
        'url'  => $url,
    ];

    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    exit();
}



/**
 * 获取系统信息
 * @return 返回array数组
 */
function getSystemInfo()
{
    $user_agent = request()->header('user-agent');
    
    if (false !== stripos($user_agent, 'win')) {
        $user_os = 'Windows';
    } elseif (false !== stripos($user_agent, 'mac')) {
        $user_os = 'MAC';
    } elseif (false !== stripos($user_agent, 'linux')) {
        $user_os = 'Linux';
    } elseif (false !== stripos($user_agent, 'unix')) {
        $user_os = 'Unix';
    } elseif (false !== stripos($user_agent, 'bsd')) {
        $user_os = 'BSD';
    } elseif (false !== stripos($user_agent, 'iPad') || false !== stripos($user_agent, 'iPhone')) {
        $user_os = 'IOS';
    } elseif (false !== stripos($user_agent, 'android')) {
        $user_os = 'Android';
    } else {
        $user_os = 'Other';
    }

    if (false !== stripos($user_agent, 'MSIE')) {
        $user_browser = 'MSIE';
    } elseif (false !== stripos($user_agent, 'Firefox')) {
        $user_browser = 'Firefox';
    } elseif (false !== stripos($user_agent, 'Chrome')) {
        $user_browser = 'Chrome';
    } elseif (false !== stripos($user_agent, 'Safari')) {
        $user_browser = 'Safari';
    } elseif (false !== stripos($user_agent, 'Opera')) {
        $user_browser = 'Opera';
    } else {
        $user_browser = 'Other';
    }
    $user_ip         = request()->ip();


    $info = [
        //服务器系统
        'server_os'           => PHP_OS,
        //服务器ip
        'server_ip'           => GetHostByName($_SERVER['SERVER_NAME']),
        //服务器环境
        'server_web'           => $_SERVER['SERVER_SOFTWARE'],
        //php版本
        'php_version'         => PHP_VERSION,
        //运行内存限制
        'memory_limit'        => ini_get('memory_limit'),
        //最大文件上传限制
        'upload_max_filesize' => ini_get('upload_max_filesize'),
        //单次上传数量限制
        'max_file_uploads'    => ini_get('max_file_uploads'),
        //最大post限制
        'post_max_size'       => ini_get('post_max_size'),
        //ThinkPHP版本
        'think_version'       => app()->version(),
        //运行模式
        'php_sapi_name'       => PHP_SAPI,
        //磁盘剩余空间
        'disk_free'           => round((disk_free_space(".") / (1024 * 1024)), 2) . 'M',
        //mysql版本
        'db_version'          => Db::query('select VERSION() as db_version')[0]['db_version'],
        //php时区
        'timezone'            => date_default_timezone_get(),
        //当前时间
        'date_time'           => date('Y-m-d H:i:s'),
        //用户IP
        'user_ip'             => $user_ip,
        //用户系统
        'user_os'             => $user_os,
        //用户浏览器
        'user_browser'        => $user_browser,

    ];
    return $info;
}


if (!function_exists('random_string')) {
    /**
     * 随机字符串生成
     * @param number $length 长度
     * @param string $type 类型
     * @param number $convert 转换大小写
     * @return string
     */
    function random_string($length = 6, $type = 'all', $convert = 0)
    {
        $config = array(
            'number' => '1234567890',
            'letter' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'string' => 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789',
            'all'    => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'
        );

        if (!isset($config[$type])) $type = 'all';
        $string = $config[$type];

        $code = '';
        $strlen = strlen($string) - 1;
        for ($i = 0; $i < $length; $i++) {
            $code .= $string{
            mt_rand(0, $strlen)};
        }
        if (!empty($convert)) {
            $code = ($convert > 0) ? strtoupper($code) : strtolower($code);
        }
        return $code;
    }
}
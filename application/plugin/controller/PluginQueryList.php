<?php

namespace app\plugin\controller;

use app\common\controller\Admin;
use QL\QueryList;

class PluginQueryList extends Admin
{
    /**
     * 显示资源列表
     */
    public function index()
    {
        $data = QueryList::get('https://www.baidu.com/s?wd=QueryList', null, [
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
                'Accept-Encoding' => 'gzip, deflate, br',
            ]
        ])->rules([
            'title'     => ['h3', 'text'],
            'link'      => ['h3>a', 'href'],
            'abstract'  => ['.c-abstract', 'text']
        ])->range('.result')->queryData();
        //dump($data);
        $this->assign('list',$data);
        return $this->fetch();
    }

}

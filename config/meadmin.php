<?php
// +----------------------------------------------------------------------
// | MEAdmin [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2020-2020 http://www.meetes.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 扯文艺的猿 <meetes@163.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 产品信息
// +----------------------------------------------------------------------

return [
    // 产品信息
    'product_name'      => 'MEAdmin 后台管理系统',
    'product_version'   => '1.0.0',
    'product_website'   => 'http://www.meetes.cn',
    'product_doc'       => 'http://doc.meetes.cn',
    'develop_team'      => '扯文艺的猿',

    // 作者信息
    'author_name'       => '扯文艺的猿',
    'author_eamil'      => 'meetes@163.com',
    'github'            => 'https://github.com/yeyangyang',
    'gitee'             => 'https://gitee.com/yeyangyang',
];

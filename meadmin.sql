/*
Navicat MySQL Data Transfer

Source Server         : 笔记本本地
Source Server Version : 80012
Source Host           : localhost:3306
Source Database       : meadmin

Target Server Type    : MYSQL
Target Server Version : 80012
File Encoding         : 65001

Date: 2020-06-12 15:44:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for me_admin_attachment
-- ----------------------------
DROP TABLE IF EXISTS `me_admin_attachment`;
CREATE TABLE `me_admin_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件名',
  `module` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模块名，由哪个模块上传的',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件路径',
  `thumb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '缩略图路径',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件链接',
  `mime` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `ext` char(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件类型',
  `size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'sha1 散列值',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `filetype` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '文件类型（1:图片2文件3音频4视频5flashs）',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `width` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '图片宽度',
  `height` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '图片高度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统附件表';

-- ----------------------------
-- Records of me_admin_attachment
-- ----------------------------
INSERT INTO `me_admin_attachment` VALUES ('4', '1', 'd3485620af16c700809df6ac0249567d.jpg', 'user', '/uploads/images/20200308/9d0fec132df5ddf4562ceb0d330bdd51.jpg', '', '', 'image/jpeg', 'jpg', '2225699', '84b155c49ba06288e14002352ccca1c9', '54a5d77a4845c7f92b3b65065465194a56423584', '1583652905', '1583652905', '1', '1', '1920', '1080');
INSERT INTO `me_admin_attachment` VALUES ('5', '1', 'timg.jpg', 'admin', '/uploads/images/20200319/cd68900fcaecb481d8ccd7c2c6e5f23e.jpg', '', '', 'image/jpeg', 'jpg', '198655', '809b4abfcd8714d4b515ff5b6d0fa496', '274bc0b0e9d1e25a1624ad34c8e9ef4465fb1d3a', '1584598217', '1584598217', '1', '1', '840', '830');
INSERT INTO `me_admin_attachment` VALUES ('24', '1', 'timg.jpg', '个人资料', '/uploads/images/20200409/6d47656c7831df49739ce2e4ecb0bbd2.jpg', '/uploads/images/20200409/thumb/6d47656c7831df49739ce2e4ecb0bbd2.jpg', '', 'image/jpeg', 'jpg', '213348', '09be3aac9149ccc6a6ba4780c84c0d87', '0a94cc254a40079a719e270deea9dc8e62c2837c', '1586409806', '1586409806', '1', '1', '840', '830');
INSERT INTO `me_admin_attachment` VALUES ('25', '1', 'alipay.png', 'config', '/uploads/images/20200409/6a3a1a2453d6c99fc8ec7913ba75050b.png', '/uploads/images/20200409/thumb/6a3a1a2453d6c99fc8ec7913ba75050b.png', '', 'image/png', 'png', '50755', '2027a6a869e44cb6f06f28fcb8206c76', 'eb5ae38a05e76eb677aeb3fb47b4108a56c63cb7', '1586411669', '1586411669', '1', '1', '230', '230');
INSERT INTO `me_admin_attachment` VALUES ('26', '1', '2X20pt.png', 'config', '/uploads/images/20200409/ea43cfe94db960696c1676962e306341.png', '/uploads/images/20200409/thumb/ea43cfe94db960696c1676962e306341.png', '', 'image/png', 'png', '248973', '45fac5af976cdb9c338b36c11d522ae1', '71c9b254647ff93323d68d90553a8fb42c64a42b', '1586411755', '1586411755', '1', '1', '1243', '1242');
INSERT INTO `me_admin_attachment` VALUES ('27', '1', '2X20pt.jpg', 'config', '/uploads/images/20200409/378accd141a9f16f336ad373a0dcf7c5.jpg', '/uploads/images/20200409/thumb/378accd141a9f16f336ad373a0dcf7c5.jpg', '', 'image/jpeg', 'jpg', '162777', 'b93f5b38bf9045ebf089ebc256c4523c', '03e34f9b519b285beca7df66b61813d80fa4d41d', '1586411765', '1586411765', '1', '1', '1243', '1242');
INSERT INTO `me_admin_attachment` VALUES ('28', '1', '3174B8EBB5EE7C4263D23E44E238DC68.jpg', 'config', '/uploads/images/20200409/058fee59e67f98d6fc656f8d53c709cb.jpg', '/uploads/images/20200409/thumb/058fee59e67f98d6fc656f8d53c709cb.jpg', '', 'image/jpeg', 'jpg', '29874', '46ce6678e38a93a8665c576f471a3c59', '8ab6d81aed4b2f79817ee13ddf92c9cf3203b673', '1586411920', '1586411920', '1', '1', '430', '430');
INSERT INTO `me_admin_attachment` VALUES ('29', '1', 'logo.png', 'config', '/uploads/images/20200409/37d46e8b97d4472fcaf87c60cc586678.png', '/uploads/images/20200409/thumb/37d46e8b97d4472fcaf87c60cc586678.png', '', 'image/png', 'png', '104515', '31fd4fd88dea19251d6210d3dc45ef9a', '6debcdaf4ae1c047e26ceae9b075fe07a392e649', '1586415055', '1586415055', '1', '1', '2033', '2033');
INSERT INTO `me_admin_attachment` VALUES ('30', '1', 'd3485620af16c700809df6ac0249567d.jpg', '个人资料', '/uploads/images/20200425/2727d4d9b21fbb2c8fe6f0baeb9b53c0.jpg', '/uploads/images/20200425/thumb/2727d4d9b21fbb2c8fe6f0baeb9b53c0.jpg', '', 'image/jpeg', 'jpg', '488892', 'e3b8858ac6c53d976a59697a1f53abb6', '3dec8d331f4ce412312a8e22e09c6bb0ce7ac728', '1587803777', '1587803777', '1', '1', '1600', '900');
INSERT INTO `me_admin_attachment` VALUES ('31', '13', 'avatar.png', 'config', '/uploads/images/20200428/50467197f4b2f64afebfea7dec846ca5.png', '/uploads/images/20200428/thumb/50467197f4b2f64afebfea7dec846ca5.png', '', 'image/png', 'png', '317988', 'dc5371618d5f80fe3e2eedc8fe0287a3', 'b12bccf142a3a9a5a3ef7117e76facef5ab0a618', '1588064604', '1588064604', '1', '1', '640', '640');
INSERT INTO `me_admin_attachment` VALUES ('32', '13', 'watermark.png', 'config', '/uploads/images/20200428/9d3658ab2a096aa116eba523926138ba.png', '/uploads/images/20200428/thumb/9d3658ab2a096aa116eba523926138ba.png', '', 'image/png', 'png', '25941', '5f6c146a546f85531da9148ddcc1c68a', '21efb837ff318d59d7b92e3fe47475a5dd5efff2', '1588064735', '1588064735', '1', '1', '145', '145');

-- ----------------------------
-- Table structure for me_admin_config
-- ----------------------------
DROP TABLE IF EXISTS `me_admin_config`;
CREATE TABLE `me_admin_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `title` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `group` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置分组',
  `group_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置分组（中文名）',
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '类型',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置值',
  `options` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置项',
  `tips` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置提示',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统配置表';

-- ----------------------------
-- Records of me_admin_config
-- ----------------------------
INSERT INTO `me_admin_config` VALUES ('1', 'web_site_name', '网站名称', 'base', '基本配置', 'text', 'MEAdmin 后台管理系统', '0', '将显示在浏览器窗口标题等位置', '1475240395', '1588600474');
INSERT INTO `me_admin_config` VALUES ('2', 'web_site_logo', '网站LOGO', 'base', '基本配置', 'image', '32', '0', '网站LOGO图片', '1475240395', '1588600474');
INSERT INTO `me_admin_config` VALUES ('3', 'web_site_favicon', '网站图标', 'base', '基本配置', 'image', '31', '0', '又叫网站收藏夹图标，它显示位于浏览器的地址栏或者标题前面，&lt;strong class=&quot;red&quot;&gt;.ico格式&lt;/strong&gt;，&lt;a href=&quot;https://www.baidu.com/s?ie=UTF-8&amp;wd=favicon&quot; target=&quot;_blank&quot;&gt;点此了解网站图标&lt;/a&gt;', '1475240395', '1588600474');
INSERT INTO `me_admin_config` VALUES ('4', 'web_site_title', '网站标题', 'base', '基本配置', 'text', 'MEAdmin 后台系统', '0', '网站标题是体现一个网站的主旨，要做到主题突出、标题简洁、连贯等特点，建议不超过28个字。网站标题是体现一个网站的主旨，要做到主题突出、标题简洁、连贯等特点，建议不超过28个字', '1475240395', '1588600474');
INSERT INTO `me_admin_config` VALUES ('5', 'web_site_keywords', '网站关键词', 'base', '基本配置', 'text', 'meetes,meadmin,MEadmin,ME,后台系统', '0', '网页内容所包含的核心搜索关键词，多个关键字请用英文逗号&quot;,&quot;分隔', '1475240395', '1588600474');
INSERT INTO `me_admin_config` VALUES ('6', 'web_site_description', '网站描述', 'base', '基本配置', 'textarea', '扯文艺的猿开发的一款后台系统。', '', '网站描述，有利于搜索引擎抓取相关信息', '1475240395', '1588600474');
INSERT INTO `me_admin_config` VALUES ('7', 'web_site_copyright', '版权信息', 'base', '基本配置', 'text', 'Copyright © 2020-2020 MEAdmin All rights reserved.', '0', '', '1475240395', '1588600474');
INSERT INTO `me_admin_config` VALUES ('8', 'web_site_icp', 'ICP备案信息', 'base', '基本配置', 'text', '浙ICP备18037011号-2', '0', '请填写ICP备案号，用于展示在网站底部，ICP备案官网：&lt;a href=&quot;http://beian.miit.gov.cn&quot; target=&quot;_blank&quot;&gt;http://beian.miit.gov.cn&lt;/a&gt;', '1475240395', '1588600474');
INSERT INTO `me_admin_config` VALUES ('9', 'web_site_statis', '站点统计代码', 'base', '基本配置', 'textarea', '', '0', '第三方流量统计代码，前台调用时请先用 htmlspecialchars_decode函数转义输出', '1475240395', '1588600474');
INSERT INTO `me_admin_config` VALUES ('20', 'admin_path', '后台管理路径（待实现）', 'system', '系统配置', 'text', 'admin.php', '0', '必须以.php为后缀', '1475240395', '1588600466');
INSERT INTO `me_admin_config` VALUES ('21', 'captcha_signin', '后台验证码开关', 'system', '系统配置', 'radio', '1', '[[0,\"关闭\"],[1,\"开启\"]]', '后台登录时是否需要验证码', '1475240395', '1588600466');
INSERT INTO `me_admin_config` VALUES ('40', 'develop_mode', '开发模式', 'develop', '开发配置', 'radio', '1', '[[0,\"关闭\"],[1,\"开启\"]]', '生产环境下一定要关闭此配置', '1475240395', '1586412100');
INSERT INTO `me_admin_config` VALUES ('41', 'app_trace', '显示页面Trace', 'develop', '开发配置', 'radio', '1', '[[0,\"否\"],[1,\"是\"]]', '生产环境下一定要关闭此配置', '1475240395', '1586412100');
INSERT INTO `me_admin_config` VALUES ('60', 'upload_file_size', '文件上传大小限制', 'upload', '上传配置', 'text', '10240', '', '0为不限制大小，单位：kb', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('61', 'upload_file_ext', '允许上传的文件后缀', 'upload', '上传配置', 'tags', 'doc,docx,xls,xlsx,ppt,pptx,pdf,wps,txt,rar,zip,gz,bz2,7z', '', '多个后缀用逗号隔开，不填写则不限制类型', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('62', 'upload_video_size', '视频上传大小限制', 'upload', '上传配置', 'text', '10240', '', '0为不限制大小，单位：kb', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('63', 'upload_video_ext', '允许上传的视频后缀', 'upload', '上传配置', 'tags', 'mp4,avi,wmv,rm,rmvb,mkv', '', '多个后缀用逗号隔开，不填写则不限制类型', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('64', 'upload_audio_size', '音频上传大小限制', 'upload', '上传配置', 'text', '10240', '', '0为不限制大小，单位：kb', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('65', 'upload_audio_ext', '允许上传的音频后缀', 'upload', '上传配置', 'tags', 'mp3,wma,wav', '', '多个后缀用逗号隔开，不填写则不限制类型', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('66', 'upload_image_size', '图片上传大小限制', 'upload', '上传配置', 'text', '10240', '', '0为不限制大小，单位：kb', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('67', 'upload_image_ext', '允许上传的图片后缀', 'upload', '上传配置', 'tags', 'jpg,jpeg,png,gif,bmp4,ico', '', '多个后缀用逗号隔开，不填写则不限制类型', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('68', 'thumb_size', '缩略图尺寸', 'upload', '上传配置', 'text', '300,300', '', '不填写则不生成缩略图，如需生成 <code>300x300</code> 的缩略图，则填写 <code>300,300</code> ，请注意，逗号必须是英文逗号', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('69', 'thumb_type', '缩略图裁剪方式', 'upload', '上传配置', 'radio', '1', '[[1,\"等比例缩放\"],[2,\"缩放后填充\"],[3,\"居中裁剪\"],[4,\"左上角裁剪\"],[5,\"右下角裁剪\"],[6,\"固定尺寸缩放\"]]', '', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('70', 'image_watermark', '图片水印开关', 'upload', '上传配置', 'radio', '0', '[[0,\"否\"],[1,\"是\"]]', '', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('71', 'image_watermark_pic', '图片水印图', 'upload', '上传配置', 'image', '29', '', '', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('72', 'image_watermark_location', '图片水印图位置', 'upload', '上传配置', 'radio', '9', '[[1,\"左上角\"],[2,\"上居中\"],[3,\"右上角\"],[4,\"左居中\"],[5,\"居中\"],[6,\"右居中\"],[7,\"左下角\"],[8,\"下居中\"],[9,\"右下角\"]]', '', '1475240395', '1588065179');
INSERT INTO `me_admin_config` VALUES ('73', 'image_watermark_opacity', '图片水印透明度', 'upload', '上传配置', 'text', '90', '0', '可设置值为0~100，数字越小，透明度越高', '1475240395', '1588065179');

-- ----------------------------
-- Table structure for me_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `me_admin_log`;
CREATE TABLE `me_admin_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL COMMENT '用户uid',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `url` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `ip` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=991 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统日志表';

-- ----------------------------
-- Records of me_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for me_admin_user
-- ----------------------------
DROP TABLE IF EXISTS `me_admin_user`;
CREATE TABLE `me_admin_user` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '电子邮箱',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户密码',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户头像',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `last_login_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后一次登录时间',
  `last_login_ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登录ip',
  `login_num` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '登陆次数',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(1启用,2禁用)',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序',
  `describe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `group_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '角色组id',
  `delete_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '删除时间',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='系统用户表';

-- ----------------------------
-- Records of me_admin_user
-- ----------------------------
INSERT INTO `me_admin_user` VALUES ('1', 'admin', '5822@qq.com', '超级管理员9', '$2y$10$2424qzXc5wwquTlgl5ElAeALO4AA01tJA.cByrj6PhXvOrHPNSYJ6', '/uploads/images/20200409/6d47656c7831df49739ce2e4ecb0bbd2.jpg', '1586815735', '1590491885', '1590491884', '127.0.0.1', '88', '1', '100', '超级管理员无法删除', '1', '0');
INSERT INTO `me_admin_user` VALUES ('2', 'admin2', '', '管理员', '$2y$10$2424qzXc5wwquTlgl5ElAeALO4AA01tJA.cByrj6PhXvOrHPNSYJ6', '/uploads/images/20200409/378accd141a9f16f336ad373a0dcf7c5.jpg', '1581845735', '1586845735', '1586845735', '127.0.0.1', '1', '1', '100', '这是管理员', '1', '0');
INSERT INTO `me_admin_user` VALUES ('3', 'admin3', '', '管理员3号', '$2y$10$2424qzXc5wwquTlgl5ElAeALO4AA01tJA.cByrj6PhXvOrHPNSYJ6', '/uploads/images/20200409/378accd141a9f16f336ad373a0dcf7c5.jpg', '1581335735', '1581335735', '0', '', '0', '2', '100', '这是管理员3', '1', '0');
INSERT INTO `me_admin_user` VALUES ('4', 'admin4', '', '管理员4', '$2y$10$2424qzXc5wwquTlgl5ElAeALO4AA01tJA.cByrj6PhXvOrHPNSYJ6', '/uploads/images/20200409/378accd141a9f16f336ad373a0dcf7c5.jpg', '1586815735', '1586845735', '1586845735', '127.0.0.1', '55', '1', '100', '超级管理员无法删除', '1', '0');
INSERT INTO `me_admin_user` VALUES ('5', 'admin5', '', '管理员5', '$2y$10$2424qzXc5wwquTlgl5ElAeALO4AA01tJA.cByrj6PhXvOrHPNSYJ6', '/uploads/images/20200409/378accd141a9f16f336ad373a0dcf7c5.jpg', '1581845735', '1586845735', '1586845735', '127.0.0.1', '1', '2', '100', '这是管理员', '1', '0');
INSERT INTO `me_admin_user` VALUES ('6', 'admin6', '', '管理员6', '$2y$10$2424qzXc5wwquTlgl5ElAeALO4AA01tJA.cByrj6PhXvOrHPNSYJ6', '/uploads/images/20200409/378accd141a9f16f336ad373a0dcf7c5.jpg', '1581335735', '1581335735', '0', '', '0', '2', '100', '这是管理员3', '1', '0');
INSERT INTO `me_admin_user` VALUES ('7', 'admin7', '', '管理员4', '$2y$10$2424qzXc5wwquTlgl5ElAeALO4AA01tJA.cByrj6PhXvOrHPNSYJ6', '/uploads/images/20200409/378accd141a9f16f336ad373a0dcf7c5.jpg', '1586815735', '1586880392', '1586845735', '127.0.0.1', '55', '1', '100', '超级管理员无法删除', '11', '1586880392');
INSERT INTO `me_admin_user` VALUES ('8', 'admin8', '', '管理员5', '$2y$10$2424qzXc5wwquTlgl5ElAeALO4AA01tJA.cByrj6PhXvOrHPNSYJ6', '/uploads/images/20200409/378accd141a9f16f336ad373a0dcf7c5.jpg', '1581845735', '1586880392', '1586845735', '127.0.0.1', '1', '1', '100', '这是管理员', '10', '1586880392');
INSERT INTO `me_admin_user` VALUES ('9', 'admin9', '', '管理员6', '$2y$10$2424qzXc5wwquTlgl5ElAeALO4AA01tJA.cByrj6PhXvOrHPNSYJ6', '/uploads/images/20200409/378accd141a9f16f336ad373a0dcf7c5.jpg', '1581335735', '1586880378', '0', '', '0', '2', '100', '这是管理员3', '0', '1586880378');
INSERT INTO `me_admin_user` VALUES ('10', 'admin11', '123123@qq.com', 'yyy', '$2y$10$LYlI5j.l9FXWPtqj1/Ar8eLP8NgV1Y6hqSbkqZCGXswatd05AB51i', '', '1586955099', '1586955099', '0', '', '0', '2', '100', '', '1', '0');
INSERT INTO `me_admin_user` VALUES ('11', 'admin12', '1231213@qq.com', '超级管理员12', '$2y$10$I2VaOP5jyfbZF/dpJrw4eecfIkOUAiUltTIKUF2HdqtG1mA/pY/K2', '', '1586955213', '1588011084', '0', '', '0', '1', '100', '法大大', '15', '0');
INSERT INTO `me_admin_user` VALUES ('12', 'admin13', '1qq@111.com', '超级管理员13', '$2y$10$ZqN5NGcAcNmiMDOobcSYY.YxZKaSnD9ed.l1DG3zXWPMEWttpdeJi', '', '1586955763', '1587918326', '1587894204', '127.0.0.1', '1', '1', '100', '1231212124', '16', '0');
INSERT INTO `me_admin_user` VALUES ('13', 'admin14', 'xlf@300c.cn', '超级管理员141', '$2y$10$h/W9Kook25VgRxYDYa85HO2PX1A5/EBqANOUb76F.2v2yRWQV8K7S', '/uploads/images/20200428/50467197f4b2f64afebfea7dec846ca5.png', '1586955836', '1588066692', '1588042106', '127.0.0.1', '7', '1', '100', '模块的应用市场，里面记录了卓远网络开发的一系列模块。可以提供用户购买、下载、安装等操作。', '12', '0');
INSERT INTO `me_admin_user` VALUES ('14', 'admin141', '582871072@qq.com', '我是1231下面的人', '$2y$10$IAcbZylzYQpNR1SEGZynkeLRXqz4FG8x/Uhbz7hOe3q0zdpKLWsZG', '', '1587828650', '1587915779', '1587915778', '127.0.0.1', '25', '2', '100', '', '13', '0');
INSERT INTO `me_admin_user` VALUES ('15', 'yyy', '5812@qq.com', '测试11', '$2y$10$7ZRQChECI3ZzBJccmtf5sOqe8t5qHVrXcxeAT5dY1YzjC8UV450Rq', '', '1587996892', '1587997769', '0', '', '0', '1', '100', '测试用的', '4', '1587997769');
INSERT INTO `me_admin_user` VALUES ('16', 'admin140', 'yy1y@300c.cn', 'admin140', '$2y$10$nYzKCoSH0mqany9JHf.qf.aaONyuQZ27SkMrVKFohkwQGfUFZB/TS', '', '1588001752', '1588001752', '0', '', '0', '1', '100', '', '12', '0');
INSERT INTO `me_admin_user` VALUES ('17', 'admin123123', 'admin123123@163.com', 'admin123123', '$2y$10$p1LVNt0UJ7bzux/PBK5UceXq79MNgadVWSzYn2U8kkmOGiWslu2US', '', '1588004185', '1588004250', '0', '', '0', '1', '100', '123', '13', '0');
INSERT INTO `me_admin_user` VALUES ('18', '1820021', '11@300c.cn', '1820021', '$2y$10$8rRDlhkshFC8jUyF40PIlOkIGh3IpLwu1KBMtDWjIMWietYuNNE.a', '', '1588006896', '1588006896', '0', '', '0', '1', '100', '', '16', '0');
INSERT INTO `me_admin_user` VALUES ('19', '170824', 'z1jq@300c.cn', '170824', '$2y$10$/gogHKFWdl1DzPrzdAYS6.wUGJiOSpM1NZ44qV7vC.dqDoOSMUgjq', '', '1588014655', '1588014655', '0', '', '0', '1', '100', '发21', '14', '0');

-- ----------------------------
-- Table structure for me_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `me_auth_group`;
CREATE TABLE `me_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户组中文名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `rules` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户组拥有的规则id， 多个规则","隔开',
  `pid` smallint(6) NOT NULL DEFAULT '0' COMMENT '父级id',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='规则用户组表';

-- ----------------------------
-- Records of me_auth_group
-- ----------------------------
INSERT INTO `me_auth_group` VALUES ('1', '超级管理员', '1', '1,13,21,32,40,2,3,373,374,375,376,368,369,370,371,372,363,364,365,366,367,50,51,52,53,4,54', '0', '拥有所有操作权限');
INSERT INTO `me_auth_group` VALUES ('2', '管理员', '1', '1,21,40,2', '1', '拥有普通管理员权限');
INSERT INTO `me_auth_group` VALUES ('3', '会员', '1', '1,377,378,379,380,21,2', '0', '普通会员权限');
INSERT INTO `me_auth_group` VALUES ('4', '扩展管理员', '1', '1,377,378,379,380,4,54', '0', '扩展管理员');
INSERT INTO `me_auth_group` VALUES ('12', '超级系统管理员', '1', '377,378,379,380,1,2,13,21,32,40,381,382,383,384,385,387,388,373,374,375,376,50,368,369,370,371,372,51,363,364,365,366,367,52,53,3', '0', '低于超级管理员的一个角色');
INSERT INTO `me_auth_group` VALUES ('13', '权限管理员', '1', '1,377,378,379,380,3,50,51,52,53,363,364,365,366,367,368,369,370,371,372,373,374,375,376', '12', '权限管理员');
INSERT INTO `me_auth_group` VALUES ('14', '系统管理员', '1', '1,377,378,379,380,2,13,21,32,40', '12', '系统管理员系统管理员');
INSERT INTO `me_auth_group` VALUES ('15', 'uiou', '1', '', '14', 'kjhfghfjh');
INSERT INTO `me_auth_group` VALUES ('16', 'fsdf', '1', '', '13', 'fsadfasdf');
INSERT INTO `me_auth_group` VALUES ('17', '我是权限组', '1', '1,377,378,379,380,3,50,51,52,53,363,364,365,366,367,368,369,370,371,372,373,374,375,376', '13', '我是权限组');

-- ----------------------------
-- Table structure for me_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `me_auth_rule`;
CREATE TABLE `me_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则唯一标识（英文）',
  `title` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为2禁用',
  `sort` smallint(10) NOT NULL DEFAULT '100' COMMENT '排序',
  `pid` smallint(6) NOT NULL DEFAULT '0' COMMENT '父级id',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型',
  `level` tinyint(1) NOT NULL COMMENT '权限节点级别',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注描述',
  `condition` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',
  `icon` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=390 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='规则权限节点表';

-- ----------------------------
-- Records of me_auth_rule
-- ----------------------------
INSERT INTO `me_auth_rule` VALUES ('1', 'admin/Index/index', '后台主页', '1', '1', '0', '2', '1', '', '', 'fa fa-home');
INSERT INTO `me_auth_rule` VALUES ('2', 'admin/System', '系统管理', '1', '2', '0', '1', '1', '', '', 'fa fa-cogs');
INSERT INTO `me_auth_rule` VALUES ('3', 'admin/Role', '权限管理', '1', '3', '0', '1', '1', '', '', 'fa fa-key');
INSERT INTO `me_auth_rule` VALUES ('4', 'admin/Extension', '扩展管理', '1', '4', '0', '1', '1', '暂搁，还没想好做什么', '', 'fa fa-diamond');
INSERT INTO `me_auth_rule` VALUES ('13', 'admin/Config/index', '系统参数配置', '1', '101', '2', '2', '2', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('21', 'admin/Log/index', '系统日志管理', '1', '102', '2', '2', '2', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('32', 'admin/Attachment/index', '附件管理', '1', '103', '2', '2', '2', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('40', 'admin/database/index', '数据库管理', '1', '104', '2', '2', '2', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('50', 'admin/Role/index', '系统角色管理', '1', '111', '3', '2', '2', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('51', 'admin/AuthRule/index', '系统权限管理', '1', '112', '3', '2', '2', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('52', 'admin/User/index', '系统用户管理', '1', '113', '3', '2', '2', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('54', 'admin/Hook/index', '钩子管理', '1', '141', '4', '2', '2', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('363', 'admin/User/add', '系统用户管理_添加', '1', '100', '52', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('364', 'admin/User/delete', '系统用户管理_删除', '1', '100', '52', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('365', 'admin/User/editStatus', '系统用户管理_修改状态', '1', '100', '52', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('366', 'admin/User/edit', '系统用户管理_修改', '1', '100', '52', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('367', 'admin/User/sort', '系统用户管理_排序', '1', '100', '52', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('368', 'admin/AuthRule/add', '系统权限管理_添加', '1', '100', '51', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('369', 'admin/AuthRule/delete', '系统权限管理_删除', '1', '100', '51', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('370', 'admin/AuthRule/editStatus', '系统权限管理_修改状态', '1', '100', '51', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('371', 'admin/AuthRule/edit', '系统权限管理_修改', '1', '100', '51', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('372', 'admin/AuthRule/sort', '系统权限管理_排序', '1', '100', '51', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('373', 'admin/Role/edit', '系统角色管理_修改', '1', '100', '50', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('374', 'admin/Role/editStatus', '系统角色管理_修改状态', '1', '100', '50', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('375', 'admin/Role/delete', '系统角色管理_删除', '1', '100', '50', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('376', 'admin/Role/add', '系统角色管理_添加', '1', '100', '50', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('377', 'admin/Index/personal', '个人信息', '1', '100', '1', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('378', 'admin/Index/password', '安全设置', '1', '100', '1', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('379', 'admin/Index/clear', '清理缓存', '1', '100', '1', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('380', 'admin/Index/search', '搜索', '1', '100', '1', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('381', 'admin/Database/repairTable', '修复表', '1', '100', '40', '3', '3', '', null, '');
INSERT INTO `me_auth_rule` VALUES ('382', 'admin/Database/optimizeTable', '优化表', '1', '100', '40', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('383', 'admin/Database/export', '备份数据库', '1', '100', '40', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('384', 'admin/Database/import', '恢复数据库', '1', '100', '40', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('385', 'admin/Database/delete', '删除备份数据库', '1', '100', '40', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('387', 'admin/Attachment/upload', '上传附件', '1', '100', '32', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('388', 'admin/Attachment/delete', '删除附件', '1', '100', '32', '3', '3', '', '', '');
INSERT INTO `me_auth_rule` VALUES ('389', 'admin/Log/delete', '删除日志', '1', '100', '21', '3', '3', '', '', '');
